# GRPC Wallet Proto
Project Wallet Proto

## Prerequisites

 - JDK 9+
 
## Build Project

**Clone project**

```sh
git clone https://bitbucket.org/castelloms/grpc-wallet-proto.git
```

**Execute this commands on the project folder to generate grpc integration**

```sh
In the folder where is pom.xml, execute:
- mvn clean install - build project with your dependencies

Add to your pom.xml dependencies, client and server grpc:

<dependency>
  <groupId>com.castello</groupId>
  <artifactId>grpc-wallet-proto</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
```
